const helpers = require("./helpers"),
  DefinePlugin = require('webpack/lib/DefinePlugin');

const nodeModules = "node_modules/";

let config = {
  entry: {
    "app": helpers.root("/src/app.ts")
  },
  output: {
    path: helpers.root("/dist/js"),
    filename: "[name].js"
  },
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".js", ".html"],
    alias: {
      'vue$': 'vue/dist/vue.common.js',
      'normalize': 'normalize.css/normalize.css'
    }
  },
  module: {
    rules: [
      {test: /\.ts$/, exclude: /node_modules/, enforce: 'pre', loader: 'tslint-loader'},
      {test: /\.ts$/, exclude: /node_modules/, loader: "awesome-typescript-loader"},
      {test: /\.scss$/, exclude: /node_modules/, loader: 'style-loader!css-loader!sass-loader'},
      {test: /\.html$/, loader: 'raw-loader', exclude: ['./src/index.html']}
    ],
  },
  plugins: [
    new DefinePlugin({
      'process.env': {
        'ENV': process.env.NODE_ENV,
        'NODE_ENV': process.env.NODE_ENV
      }
    })
  ]
};

module.exports = config;
