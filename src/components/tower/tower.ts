import Vue from "vue";
import {Component, Prop} from "vue-property-decorator";
import "./tower.scss";
const template = require("./tower.html");

@Component({
  template: template,
})
export class HanoiTowerComponent extends Vue {

  @Prop
  private diskNumber: number;

  private rods = [[], [], []];

  //noinspection JSUnusedLocalSymbols
  private created(): void {
    this.rods[0] = this.generateDisks(this.diskNumber);
  }

  //noinspection JSMethodCanBeStatic
  private generateDisks(diskNumber: number): {width: number}[] {
    const disks = [];
    for (let i = 0; i < diskNumber; i++) {
      disks.push({ width: i + 1 });
    }
    return disks;
  }

}

Vue.component("CmHanoiTower", HanoiTowerComponent);
