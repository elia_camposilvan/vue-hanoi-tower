import Vue from "vue";
import "./rod.scss";
import {Component, Prop} from "vue-property-decorator";
const template = require("./rod.html");

@Component({
    template: template
})
export class HanoiRodComponent extends Vue {

  @Prop
  private disks: {width: number}[];

  private created(): void {
    console.log("this.disks: ", this.disks);
  }

}

Vue.component("CmHanoiRod", HanoiRodComponent);
