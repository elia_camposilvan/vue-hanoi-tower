import * as Vue from "vue";
import "style-loader!css-loader!normalize";

import "./components/app/app";
import "./components/tower/tower";
import "./components/rod/rod";
import "./components/disk/disk";

new Vue({
  el: "#app"
});
